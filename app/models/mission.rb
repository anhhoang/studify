class Mission < ActiveRecord::Base
  attr_accessible :description, :order
  has_many :users
end
