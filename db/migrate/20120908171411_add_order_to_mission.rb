class AddOrderToMission < ActiveRecord::Migration
  def change
    add_column :missions, :order, :integer
  end
end
