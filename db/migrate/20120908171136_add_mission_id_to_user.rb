class AddMissionIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :mission_id, :integer, :default => 1
  end
end
